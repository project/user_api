<?php

declare(strict_types=1);

namespace Drupal\Tests\user_api_passwordless\Kernel;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\Core\Url;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\rest\Entity\RestResourceConfig;
use Drupal\Tests\user_api\Kernel\UserApiTestTrait;
use Drupal\user\UserInterface;
use Drupal\user_api_passwordless\Event\InitUnsetPasswordEvent;
use Drupal\user_api_passwordless\UserApiPasswordlessEvents;

/**
 * InitUnsetPassword test.
 *
 * @group user_api_passwordless
 */
class InitUnsetPasswordTest extends EntityKernelTestBase {

  use UserApiTestTrait;
  use AssertMailTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'rest',
    'serialization',
    'user_api',
    'user_api_passwordless',
  ];

  /**
   * The URL to the resource.
   *
   * @var \Drupal\Core\Url
   */
  protected $url;

  /**
   * The kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * User settings config instance.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $userSettings;

  /**
   * The user.
   */
  protected UserInterface $user;

  /**
   * The user password.
   */
  protected string $password = 'password';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_test');
    $this->installConfig(['user']);

    $this->setUpCurrentUser();

    RestResourceConfig::create([
      'id' => 'user_api_passwordless_init_unset_password',
      'plugin_id' => 'user_api_passwordless_init_unset_password',
      'granularity' => RestResourceConfig::RESOURCE_GRANULARITY,
      'configuration' => [
        'methods' => ['POST'],
        'formats' => ['json'],
        'authentication' => ['cookie'],
      ],
    ])->save();

    $this->userSettings = $this->config('user.settings');

    $this->userSettings
      ->set('register', UserInterface::REGISTER_VISITORS)
      ->set('verify_mail', TRUE)
      ->save();

    $this->url = Url::fromRoute('rest.user_api_passwordless_init_unset_password.POST');
    $this->httpKernel = $this->container->get('http_kernel');

    $this->user = $this->drupalCreateUser([
      'restful post user_api_passwordless_init_unset_password',
    ]);
    $this->user->setPassword($this->password)->save();
    $this->setCurrentUser($this->user);
  }

  /**
   * Test init set password.
   *
   * @test
   */
  public function testInitUnsetPassword() {
    $secondUser = $this->drupalCreateUser();

    $payload = [
      'email' => $this->user->getEmail(),
    ];

    // Invalid payload.
    $request = $this->createJsonRequest('POST', $this->url->toString(), []);
    $response = $this->httpKernel->handle($request);
    $this->assertEquals(422, $response->getStatusCode(), $response->getContent());

    $count = count($this->getMails());
    $this->assertEquals(0, $count);

    // FAILURE - Invalid email.
    $request = $this->createJsonRequest('POST', $this->url->toString(), ['email' => 'wrong@example.com']);
    $response = $this->httpKernel->handle($request);
    $this->assertEquals(400, $response->getStatusCode(), $response->getContent());

    $count = count($this->getMails());
    $this->assertEquals(0, $count);

    // FAILURE - Valid email, but from other user.
    $request = $this->createJsonRequest('POST', $this->url->toString(), ['email' => $secondUser->getEmail()]);
    $response = $this->httpKernel->handle($request);
    $this->assertEquals(403, $response->getStatusCode(), $response->getContent());

    $count = count($this->getMails());
    $this->assertEquals(0, $count);

    // SUCCESS.
    $request = $this->createJsonRequest('POST', $this->url->toString(), $payload);
    $response = $this->httpKernel->handle($request);
    $this->assertEquals(200, $response->getStatusCode(), $response->getContent());

    $count = count($this->getMails());
    $this->assertEquals(1, $count);
  }

  /**
   * Test abort via event.
   */
  public function testEventAbort() {
    /** @var \Symfony\Component\EventDispatcher\EventDispatcher $eventDispatcher */
    $eventDispatcher = $this->container->get('event_dispatcher');

    $eventDispatcher->addListener(UserApiPasswordlessEvents::INIT_UNSET_PASSWORD, function (InitUnsetPasswordEvent $event) {
      $event->abort('test', 'test', 500);
    });

    $payload = [
      'email' => $this->user->getEmail(),
    ];

    $request = $this->createJsonRequest('POST', $this->url->toString(), $payload);
    $response = $this->httpKernel->handle($request);
    $this->assertEquals(500, $response->getStatusCode(), $response->getContent());
    $this->assertStringContainsString("test", $response->getContent());
  }

}
