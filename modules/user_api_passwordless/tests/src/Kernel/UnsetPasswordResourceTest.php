<?php

declare(strict_types=1);

namespace Drupal\Tests\user_api_passworldess\Kernel;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\Core\Url;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\rest\Entity\RestResourceConfig;
use Drupal\Tests\user_api\Kernel\UserApiTestTrait;
use Drupal\user\UserInterface;
use Drupal\user_api_passwordless\Event\UnsetPasswordEvent;
use Drupal\user_api_passwordless\UserApiPasswordlessEvents;
use Drupal\verification_hash\VerificationHashManager;

/**
 * UnsetPasswordResource test.
 *
 * @group user_api_passwordless
 */
class UnsetPasswordResourceTest extends EntityKernelTestBase {

  use UserApiTestTrait;
  use AssertMailTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'rest',
    'serialization',
    'user_api',
    'user_api_passwordless',
    'user_api_test',
    'verification',
    'verification_hash',
  ];

  /**
   * The URL to the resource.
   *
   * @var \Drupal\Core\Url
   */
  protected $url;

  /**
   * The kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * User settings config instance.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $userSettings;

  /**
   * The user.
   */
  protected UserInterface $user;

  /**
   * The user password.
   */
  protected string $password = 'password';

  /**
   * The hash manager.
   */
  protected VerificationHashManager $hashManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_test');
    $this->installConfig(['user']);

    $this->setUpCurrentUser();

    RestResourceConfig::create([
      'id' => 'user_api_passwordless_unset_password',
      'plugin_id' => 'user_api_passwordless_unset_password',
      'granularity' => RestResourceConfig::RESOURCE_GRANULARITY,
      'configuration' => [
        'methods' => ['POST'],
        'formats' => ['json'],
        'authentication' => ['cookie'],
      ],
    ])->save();

    $this->userSettings = $this->config('user.settings');

    $this->url = Url::fromRoute('rest.user_api_passwordless_unset_password.POST');
    $this->httpKernel = $this->container->get('http_kernel');

    $this->user = $this->drupalCreateUser([
      'restful post user_api_passwordless_unset_password',
    ]);
    $this->user->setPassword($this->password)->save();
    $this->setCurrentUser($this->user);

    $this->hashManager = $this->container->get('verification_hash.manager');
  }

  /**
   * Test change password with old password.
   */
  public function testUnsetPasswordWithOldPassword() {
    $content = [
      'currentPassword' => $this->password,
    ];

    $request = $this->createJsonRequest('POST', $this->url->toString(), $content);
    $response = $this->httpKernel->handle($request);
    $this->assertEquals(200, $response->getStatusCode());

    $this->assertUserPasswordEmpty($this->user);
  }

  /**
   * Test change password with hash.
   */
  public function testUnsetPasswordWithHash() {
    $timestamp = \Drupal::time()->getRequestTime();
    $hash = $this->hashManager->createHash($this->user, 'unset-password', $timestamp);

    $request = $this->createJsonRequest('POST', $this->url->toString(), []);
    $request->headers->set('X-Verification-Hash', sprintf('%s$$%s', $hash, $timestamp));
    $response = $this->httpKernel->handle($request);
    $this->assertEquals(200, $response->getStatusCode());

    $this->assertUserPasswordEmpty($this->user);

    // Invalid hash.
    $request = $this->createJsonRequest('POST', $this->url->toString(), []);
    $request->headers->set('X-Verification-Hash', sprintf('%s$$%s', 'invalid-hash', $timestamp));
    $response = $this->httpKernel->handle($request);
    $this->assertEquals(400, $response->getStatusCode());
  }

  /**
   * Test invalid payload.
   */
  public function testInvalidPayload() {
    $content = [
      'currentPassword' => FALSE,
    ];

    $request = $this->createJsonRequest('POST', $this->url->toString(), $content);
    $response = $this->httpKernel->handle($request);
    $this->assertEquals(422, $response->getStatusCode());
  }

  /**
   * Test abort via event.
   */
  public function testEventAbort() {
    /** @var \Symfony\Component\EventDispatcher\EventDispatcher $eventDispatcher */
    $eventDispatcher = $this->container->get('event_dispatcher');

    $eventDispatcher->addListener(UserApiPasswordlessEvents::UNSET_PASSWORD, function (UnsetPasswordEvent $event) {
      $event->abort('test', 'test', 500);
    });

    $content = [
      'currentPassword' => $this->password,
    ];

    $request = $this->createJsonRequest('POST', $this->url->toString(), $content);
    $response = $this->httpKernel->handle($request);
    $this->assertEquals(500, $response->getStatusCode(), $response->getContent());
    $this->assertStringContainsString("test", $response->getContent());
  }

}
