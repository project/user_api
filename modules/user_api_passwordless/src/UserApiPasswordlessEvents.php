<?php

declare(strict_types=1);

namespace Drupal\user_api_passwordless;

/**
 * Defines all events in user_api_passwordless.
 */
final class UserApiPasswordlessEvents {

  /**
   * Event name prefix.
   */
  private const PREFIX = 'user_api_passwordless_';

  /**
   * Abort init unset password action.
   */
  public const INIT_UNSET_PASSWORD = self::PREFIX . 'init_unset_password';

  /**
   * Abort unset password action.
   */
  public const UNSET_PASSWORD = self::PREFIX . 'unset_password';

}
