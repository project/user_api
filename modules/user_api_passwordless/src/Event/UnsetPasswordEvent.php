<?php

declare(strict_types=1);

namespace Drupal\user_api_passwordless\Event;

use Drupal\user\UserInterface;
use Drupal\user_api\Event\UserApiEventBase;
use Drupal\user_api_passwordless\UserApiPasswordlessEvents;

/**
 * Event when the unset password resource is requested.
 */
class UnsetPasswordEvent extends UserApiEventBase {

  /**
   * Construct new UnsetPasswordEvent.
   */
  public function __construct(
    public readonly UserInterface $user,
    public readonly array $data,
    public readonly bool $verified,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getName(): string {
    return UserApiPasswordlessEvents::UNSET_PASSWORD;
  }

}
