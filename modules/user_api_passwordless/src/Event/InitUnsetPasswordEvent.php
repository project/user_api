<?php

declare(strict_types=1);

namespace Drupal\user_api_passwordless\Event;

use Drupal\user\UserInterface;
use Drupal\user_api\Event\UserApiEventBase;
use Drupal\user_api_passwordless\UserApiPasswordlessEvents;

/**
 * Event when the init unset password resource is requested.
 */
class InitUnsetPasswordEvent extends UserApiEventBase {

  /**
   * Construct new InitUnsetPasswordEvent.
   */
  public function __construct(
    public readonly UserInterface $user,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getName(): string {
    return UserApiPasswordlessEvents::INIT_UNSET_PASSWORD;
  }

}
