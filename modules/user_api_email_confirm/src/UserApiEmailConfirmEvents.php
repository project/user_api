<?php

declare(strict_types=1);

namespace Drupal\user_api_email_confirm;

/**
 * Defines all events in user_api_email_confirm.
 */
final class UserApiEmailConfirmEvents {

  /**
   * Event name prefix.
   */
  private const PREFIX = 'user_api_email_confirm_';

  /**
   * Abort init unset password action.
   */
  public const INIT_SET_EMAIL = self::PREFIX . 'init_set_email';

  /**
   * Abort unset password action.
   */
  public const SET_EMAIL = self::PREFIX . 'set_email';

}
