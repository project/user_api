<?php

declare(strict_types=1);

namespace Drupal\user_api_email_confirm\Event;

use Drupal\user\UserInterface;
use Drupal\user_api\Event\UserApiEventBase;
use Drupal\user_api_email_confirm\UserApiEmailConfirmEvents;

/**
 * Event when the init set email resource is requested.
 */
class InitSetEmailEvent extends UserApiEventBase {

  /**
   * Construct new InitSetEmailEvent.
   */
  public function __construct(
    public readonly UserInterface $user,
    public readonly string $newEmail,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getName(): string {
    return UserApiEmailConfirmEvents::INIT_SET_EMAIL;
  }

}
