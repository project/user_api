<?php

declare(strict_types=1);

namespace Drupal\user_api\Plugin\rest\resource;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rest\Attribute\RestResource;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\user\Plugin\rest\resource\UserRegistrationResource;
use Drupal\user\UserInterface;
use Drupal\user_api\Event\AdvancedUserRegistrationEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Wunderwerk\JsonApiError\JsonApiError;
use Wunderwerk\JsonApiError\JsonApiErrorResponse;

/**
 * Provides a resource to handle user account creation.
 *
 * The fields that are validated for registration can be configured by changing
 * the user_api.register.fields_to_validate service parameter.
 *
 * This enables having different fields be required for initial
 * account creation.
 */
#[RestResource(
  id: "user_api_user_registration",
  label: new TranslatableMarkup('Advanced User Registration'),
  serialization_class: "Drupal\user\Entity\User",
  uri_paths: [
    "create" => "/user-api/register",
  ]
)]
class AdvancedUserRegistrationResource extends UserRegistrationResource {

  const HEADER_DISABLE_EMAIL_NOTIFICATION = 'X-Disable-Email-Notification';
  const HEADER_DISABLE_ACCOUNT_ACTIVATION = 'X-Disable-Account-Activation';

  /**
   * The current request.
   */
  protected ?Request $currentRequest;

  /**
   * Constructs a new UserRegistrationResource instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    RequestStack $requestStack,
    LoggerInterface $logger,
    ImmutableConfig $userSettings,
    AccountInterface $currentUser,
    PasswordGeneratorInterface $passwordGenerator,
    protected EventDispatcherInterface $eventDispatcher,
    protected array $fieldsToValidate,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $userSettings, $currentUser, $passwordGenerator);

    $this->currentRequest = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('request_stack'),
      $container->get('logger.factory')->get('rest'),
      $container->get('config.factory')->get('user.settings'),
      $container->get('current_user'),
      $container->get('password_generator'),
      $container->get('event_dispatcher'),
      $container->getParameter('user_api.register.fields_to_validate'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function post(?UserInterface $account = NULL) {
    // Validate user entity.
    // This is to create a better error response than the core
    // response which just returns a string with the errors.
    $violations = $this->validateUser($account);
    if (!empty($violations)) {
      $errors = [];

      foreach ($violations as $field => $violation) {
        $constraintName = array_reverse(explode('\\', $violation['constraint']))[0];

        $errors[] = new JsonApiError(
          status: 422,
          code: 'validation_failed',
          title: (string) $violation['message'],
          source: [
            'pointer' => '/' . $field . '/value',
          ],
          meta: [
            'constraint' => $constraintName,
          ]
        );

        if (str_contains($violation['constraint'], 'Unique')) {
          /** @var \Drupal\user\UserInterface $user */
          $user = $this->loadUserByField($field, $account);

          if ($user && $user->getLastAccessedTime() === "0") {
            $user->delete();
            return $this->post($account);
          }
        }
      }

      return new JsonApiErrorResponse($errors);
    }

    // The core registration resource (the parent class) does not activate
    // the user account when email verification is enabled,
    // but admin approval is not.
    // This is not the same behaviour as the core registration form!
    // So we need to re-implement this behaviour here.
    // The following code is copied and altered from the
    // parent implementation.
    // Once the core class fixes it's implementation,
    // the below code can be removed.
    $this->ensureAccountCanRegister($account);

    // Dispatch event.
    $event = new AdvancedUserRegistrationEvent($account);
    $this->eventDispatcher->dispatch($event, $event::getName());
    if ($event->isAborted()) {
      return $event->getResponse();
    }

    // Only activate new users if visitors are allowed to register and no email
    // verification required AND if activation is not explicitly
    // disabled via request.
    if (
      $this->userSettings->get('register') == UserInterface::REGISTER_VISITORS &&
      $this->currentRequest->headers->get(self::HEADER_DISABLE_ACCOUNT_ACTIVATION) !== '1'
    ) {
      $account->activate();
    }
    else {
      $account->block();
    }

    $this->checkEditFieldAccess($account);

    // Make sure that the user entity is valid (email and name are valid).
    // $this->validate($account);
    // Create the account.
    $account->save();

    $this->sendEmailNotifications($account);

    return new ModifiedResourceResponse($account, 200);
  }

  /**
   * Load user by given field.
   *
   * @param string $fieldType
   *   The field type.
   * @param \Drupal\user\UserInterface $account
   *   The user account entity.
   *
   * @return \Drupal\user\UserInterface|null
   *   The user entity or null if none found.
   */
  protected function loadUserByField(string $fieldType, UserInterface $account) {
    $results = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties([$fieldType => $account->get($fieldType)->value]);

    return reset($results);
  }

  /**
   * Validate user entity.
   *
   * This method provides more detailed validation errors than the core
   * UnprocessableEntityHttpException.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account entity.
   *
   * @return array
   *   An array of validation errors.
   */
  protected function validateUser(UserInterface $account) {
    $violations = $account->validate();
    $errors = [];

    if ($violations->count() > 0) {
      /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
      foreach ($violations as $violation) {
        $errors[$violation->getPropertyPath()] = [
          'message' => $violation->getMessage(),
          'constraint' => get_class($violation->getConstraint()),
        ];
      }
    }

    return array_intersect_key($errors, array_flip($this->fieldsToValidate));
  }

  /**
   * {@inheritdoc}
   */
  protected function sendEmailNotifications(UserInterface $account) {
    // Skip email notifications if explicitly disabled via request.
    if ($this->currentRequest->headers->get(self::HEADER_DISABLE_EMAIL_NOTIFICATION) === '1') {
      return;
    }

    return parent::sendEmailNotifications($account);
  }

}
