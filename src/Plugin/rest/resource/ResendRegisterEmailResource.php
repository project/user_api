<?php

declare(strict_types=1);

namespace Drupal\user_api\Plugin\rest\resource;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rest\Attribute\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\UserInterface;
use Drupal\user_api\ErrorCode;
use Drupal\user_api\Event\ResendRegisterEmailEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Wunderwerk\HttpApiUtils\HttpApiValidationTrait;
use Wunderwerk\JsonApiError\JsonApiErrorResponse;

/**
 * Provides a resource to resend the register verification email.
 */
#[RestResource(
  id: 'user_api_resend_register_email',
  label: new TranslatableMarkup('Resend register email'),
  uri_paths: [
    'create' => '/user-api/register/resend-email',
  ],
)]
class ResendRegisterEmailResource extends ResourceBase {

  use HttpApiValidationTrait;

  /**
   * Request payload schema.
   */
  protected array $schema = [
    'type' => 'object',
    'properties' => [
      'email' => [
        'type' => 'string',
        'format' => 'email',
      ],
    ],
    'required' => ['email'],
  ];

  /**
   * The user entity.
   */
  protected UserInterface $user;

  /**
   * Constructs a new object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    protected AccountProxyInterface $currentUser,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ImmutableConfig $userSettings,
    protected EventDispatcherInterface $eventDispatcher,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('config.factory')->get('user.settings'),
      $container->get('event_dispatcher'),
    );
  }

  /**
   * Responds to POST requests.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response indicating success or failure.
   */
  public function post(Request $request) {
    $jsonBody = $request->getContent();
    $data = Json::decode($jsonBody);

    $result = $this->validateArray($data, $this->schema);
    if (!$result->isValid()) {
      return $result->getResponse();
    }

    $result = $this->entityTypeManager->getStorage('user')->loadByProperties([
      'mail' => $data['email'],
    ]);
    if (empty($result)) {
      return JsonApiErrorResponse::fromError(
        status: 400,
        code: ErrorCode::InvalidEmail->getCode(),
        title: 'Invalid email address.'
      );
    }

    /** @var \Drupal\user\UserInterface $user */
    $user = reset($result);

    return $this->handleRegister($user);
  }

  /**
   * Handles registration verification email resend.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity.
   */
  protected function handleRegister(UserInterface $user) {
    if ($this->userSettings->get('register') !== UserInterface::REGISTER_VISITORS) {
      return JsonApiErrorResponse::fromError(
        status: 500,
        code: ErrorCode::ResendMailVisitorAccountCreationDisabled->getCode(),
        title: 'Registration is disabled.'
      );
    }

    if (!$this->userSettings->get('verify_mail')) {
      return JsonApiErrorResponse::fromError(
        status: 500,
        code: ErrorCode::ResendMailRegisterVerifyMailDisabled->getCode(),
        title: 'Email verification is disabled.'
      );
    }

    // Make sure not to resend verification email if user is already verified.
    if ($user->getLastAccessedTime() !== "0") {
      return JsonApiErrorResponse::fromError(
        status: 400,
        code: ErrorCode::AlreadyVerified->getCode(),
        title: 'Account is already verified!'
      );
    }

    // Dispatch event.
    $event = new ResendRegisterEmailEvent($user);
    $this->eventDispatcher->dispatch($event, $event::getName());
    if ($event->isAborted()) {
      return $event->getResponse();
    }

    _user_mail_notify('register_no_approval_required', $user);

    return $this->createSuccessResponse();
  }

  /**
   * Create a success response.
   */
  protected function createSuccessResponse() {
    return new JsonResponse([
      'status' => 'success',
    ]);
  }

}
