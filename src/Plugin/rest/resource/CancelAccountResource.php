<?php

declare(strict_types=1);

namespace Drupal\user_api\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rest\Attribute\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\UserInterface;
use Drupal\user_api\ErrorCode;
use Drupal\user_api\Event\CancelAccountEvent;
use Drupal\verification\RequestVerifierInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Wunderwerk\JsonApiError\JsonApiErrorResponse;

/**
 * Provides a resource to cancel a user's account.
 */
#[RestResource(
  id: "user_api_cancel_account",
  label: new TranslatableMarkup('Cancel Account'),
  uri_paths: [
    "create" => "/user-api/cancel-account",
  ]
)]
class CancelAccountResource extends ResourceBase {

  /**
   * Constructs a new OneTimeLoginResource object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    protected AccountProxyInterface $currentUser,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ConfigFactoryInterface $configFactory,
    protected RequestVerifierInterface $verifier,
    protected EventDispatcherInterface $eventDispatcher,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get(RequestVerifierInterface::class),
      $container->get('event_dispatcher'),
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response indicating success or failure.
   */
  public function post(Request $request) {
    $user = $this->getCurrentUser();
    if (!$user || !$user->isAuthenticated()) {
      return JsonApiErrorResponse::fromError(
        status: 403,
        code: ErrorCode::Unauthenticated->getCode(),
        title: 'Unauthenticated',
        detail: 'You are not authenticated.',
      );
    }

    $result = $this->verifier->verifyOperation($request, 'cancel-account', $user);
    if ($response = $result->toErrorResponse()) {
      return $response;
    }

    // Dispatch event.
    $event = new CancelAccountEvent($user);
    $this->eventDispatcher->dispatch($event, $event::getName());
    if ($event->isAborted()) {
      return $event->getResponse();
    }

    return $this->cancelAccount($user);
  }

  /**
   * Cancel the given user account.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account to cancel.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  protected function cancelAccount(UserInterface $user): Response {
    $cancelMethod = $this->configFactory->get('user.settings')->get('cancel_method');

    user_cancel([], $user->id(), $cancelMethod);

    // Since user_cancel() is not invoked via Form API, batch processing
    // needs to be invoked manually.
    $batch =& batch_get();
    // Mark this batch as non-progressive to bypass the progress bar and
    // redirect.
    $batch['progressive'] = FALSE;
    batch_process();

    return new JsonResponse([
      'status' => 'success',
    ]);
  }

  /**
   * Loads the user entity for the current user.
   *
   * @return \Drupal\user\UserInterface|null
   *   The user entity.
   */
  protected function getCurrentUser(): ?UserInterface {
    return $this->entityTypeManager->getStorage('user')->load(
      $this->currentUser->id()
    );
  }

}
