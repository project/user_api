<?php

declare(strict_types=1);

namespace Drupal\user_api\Plugin\rest\resource;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Password\PasswordInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rest\Attribute\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\UserInterface;
use Drupal\user_api\ErrorCode;
use Drupal\user_api\Event\SetPasswordEvent;
use Drupal\verification\Result\VerificationResult;
use Drupal\verification\RequestVerifierInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Wunderwerk\HttpApiUtils\HttpApiValidationTrait;
use Wunderwerk\JsonApiError\JsonApiErrorResponse;

/**
 * Provides a resource to set a user's password.
 */
#[RestResource(
  id: 'user_api_set_password',
  label: new TranslatableMarkup('Set user password'),
  uri_paths: [
    'create' => '/user-api/set-password',
  ],
)]
class SetPasswordResource extends ResourceBase {

  use HttpApiValidationTrait;

  /**
   * Request payload schema.
   */
  protected array $schema = [
    'type' => 'object',
    'properties' => [
      'newPassword' => [
        'type' => 'string',
      ],
      'currentPassword' => [
        'type' => 'string',
      ],
    ],
    'required' => ['newPassword'],
  ];

  /**
   * The user entity.
   */
  protected UserInterface $user;

  /**
   * Constructs a new OneTimeLoginResource object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    protected AccountProxyInterface $currentUser,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected PasswordInterface $passwordChecker,
    protected RequestVerifierInterface $verifier,
    protected EventDispatcherInterface $eventDispatcher,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('password'),
      $container->get(RequestVerifierInterface::class),
      $container->get('event_dispatcher'),
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response indicating success or failure.
   */
  public function post(Request $request) {
    $user = $this->getCurrentUser();
    if (!$user || !$user->isAuthenticated()) {
      return JsonApiErrorResponse::fromError(
        status: 403,
        code: ErrorCode::Unauthenticated->getCode(),
        title: 'Unauthenticated',
        detail: 'You are not authenticated.',
      );
    }
    $this->user = $user;

    // Validate payload.
    $payload = $request->getContent();
    $data = Json::decode($payload);

    $result = $this->validateArray($data, $this->schema);
    if (!$result->isValid()) {
      return $result->getResponse();
    }

    // Verify the supported operations.
    /** @var \Drupal\verification\Result\VerificationResult $verifyResult */
    $verifyResult = array_reduce(['set-password', 'register'], function ($carry, $operation) use ($request, $user) {
      /** @var \Drupal\verification\Result\VerificationResult $carry */
      if ($carry->ok) {
        return $carry;
      }

      $currentResult = $this->verifier->verifyOperation($request, $operation, $user);

      // If current result is unhandled, but previous was an error, return
      // previous.
      if ($currentResult->unhandled && $carry->err) {
        return $carry;
      }

      return $currentResult;
    }, VerificationResult::unhandled());

    $newPassword = $data['newPassword'];

    // Password can be changed.
    if ($verifyResult->ok || array_key_exists('currentPassword', $data)) {
      // Dispatch event.
      $event = new SetPasswordEvent($user);
      $this->eventDispatcher->dispatch($event, $event::getName());
      if ($event->isAborted()) {
        return $event->getResponse();
      }

      // Handle update if verified.
      if ($verifyResult->ok) {
        return $this->setPassword($newPassword);
      }

      return $this->handlePasswordUpdateWithCurrentPassword($newPassword, $data['currentPassword']);
    }
    // Verification error.
    elseif ($verifyResult->err) {
      return $verifyResult->toErrorResponse();
    }

    return JsonApiErrorResponse::fromError(
      status: 400,
      code: ErrorCode::PasswordUpdateFailed->getCode(),
      title: 'Could not update password',
      detail: 'The request is neither verified to update the password directly, nor was the currentPassword supplied in the request payload.',
    );
  }

  /**
   * Handle password update with current password.
   *
   * @param string $newPassword
   *   The new password.
   * @param string $currentPassword
   *   The current password.
   */
  protected function handlePasswordUpdateWithCurrentPassword(string $newPassword, string $currentPassword) {
    // Check current password, if user updates an existing one.
    if ($currentPassHash = $this->user->getPassword()) {
      // Check against currently set password.
      if (!$this->passwordChecker->check($currentPassword, $currentPassHash)) {
        return JsonApiErrorResponse::fromError(
          status: 400,
          code: ErrorCode::CurrentPasswordInvalid->getCode(),
          title: 'Incorrect current password',
        );
      }
    }

    return $this->setPassword($newPassword);
  }

  /**
   * Sets the password on the user entity.
   *
   * @param string $newPassword
   *   The new password.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response indicating success.
   */
  protected function setPassword(string $newPassword) {
    $this->user->setPassword($newPassword);
    $this->user->save();

    return new JsonResponse([
      'status' => 'success',
    ]);
  }

  /**
   * Loads the user entity for the current user.
   *
   * @return \Drupal\user\UserInterface|null
   *   The user entity.
   */
  protected function getCurrentUser(): ?UserInterface {
    return $this->entityTypeManager->getStorage('user')->load(
      $this->currentUser->id()
    );
  }

}
