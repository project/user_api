<?php

declare(strict_types=1);

namespace Drupal\user_api;

/**
 * Defines the error codes for this module.
 */
enum ErrorCode: string {

  private const PREFIX = 'user_api';

  case Unauthenticated = 'unauthenticated';
  case InvalidEmail = 'invalid_email';
  case EmailNotMatching = 'email_not_matching';
  case InvalidOperation = 'invalid_operation';
  case AlreadyVerified = 'already_verified';
  case PasswordUpdateFailed = 'password_update_failed';
  case CurrentPasswordInvalid = 'current_password_invalid';
  case EmailSameAsCurrent = 'email_same_as_current';
  case EmailAlreadyExists = 'email_already_exists';

  case ResendMailVisitorAccountCreationDisabled = 'resend_mail_visitor_account_creation_disabled';
  case ResendMailRegisterVerifyMailDisabled = 'resend_mail_register_verify_mail_disabled';
  case ResendMailAlreadyCanceled = 'resend_mail_already_canceled';
  case EmailVerificationDisabled = 'email_verification_disabled';

  /**
   * Returns the error code prefixed with the module name.
   *
   * @return string
   *   The error code.
   */
  public function getCode(): string {
    return self::PREFIX . ':' . $this->value;
  }

}
