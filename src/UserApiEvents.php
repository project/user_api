<?php

declare(strict_types=1);

namespace Drupal\user_api;

/**
 * Defines all events in user_api.
 */
final class UserApiEvents {

  /**
   * Event name prefix.
   */
  private const PREFIX = 'user_api_';

  /**
   * Abort advanced user registration action.
   */
  public const ADVANCED_USER_REGISTRATION = self::PREFIX . 'advanced_user_registration';

  /**
   * Abort cancel account action.
   */
  public const CANCEL_ACCOUNT = self::PREFIX . 'cancel_account';

  /**
   * Abort init account cancel action.
   */
  public const INIT_CANCEL_ACCOUNT = self::PREFIX . 'init_cancel_account';

  /**
   * Abort init set password action.
   */
  public const INIT_SET_PASSWORD = self::PREFIX . 'init_set_password';

  /**
   * Abort resend register email action.
   */
  public const RESEND_REGISTER_EMAIL = self::PREFIX . 'resend_register_email';

  /**
   * Abort set password action.
   */
  public const SET_PASSWORD = self::PREFIX . 'set_password';

}
