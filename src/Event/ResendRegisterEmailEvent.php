<?php

declare(strict_types=1);

namespace Drupal\user_api\Event;

use Drupal\user\UserInterface;
use Drupal\user_api\UserApiEvents;

/**
 * Event when the resend register email resource is requested.
 */
class ResendRegisterEmailEvent extends UserApiEventBase {

  /**
   * Construct new SetPasswordEvent.
   */
  public function __construct(
    public readonly ?UserInterface $user,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getName(): string {
    return UserApiEvents::RESEND_REGISTER_EMAIL;
  }

}
