<?php

declare(strict_types=1);

namespace Drupal\user_api\Event;

use Drupal\user\UserInterface;
use Drupal\user_api\UserApiEvents;

/**
 * Event when the advanced user registration resource is requested.
 */
class AdvancedUserRegistrationEvent extends UserApiEventBase {

  /**
   * Construct new AdvancedUserRegistrationEvent.
   */
  public function __construct(
    public readonly ?UserInterface $user,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getName(): string {
    return UserApiEvents::ADVANCED_USER_REGISTRATION;
  }

}
