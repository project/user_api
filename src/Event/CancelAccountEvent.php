<?php

declare(strict_types=1);

namespace Drupal\user_api\Event;

use Drupal\user\UserInterface;
use Drupal\user_api\UserApiEvents;

/**
 * Event when the cancel account resource is requested.
 */
class CancelAccountEvent extends UserApiEventBase {

  /**
   * Construct new CancelAccountEvent.
   */
  public function __construct(
    public readonly ?UserInterface $user,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getName(): string {
    return UserApiEvents::CANCEL_ACCOUNT;
  }

}
