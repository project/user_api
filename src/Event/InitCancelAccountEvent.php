<?php

declare(strict_types=1);

namespace Drupal\user_api\Event;

use Drupal\user\UserInterface;
use Drupal\user_api\UserApiEvents;

/**
 * Event when the init cancel account resource is requested.
 */
class InitCancelAccountEvent extends UserApiEventBase {

  /**
   * Construct new InitCancelAccountEvent.
   */
  public function __construct(
    public readonly ?UserInterface $user,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getName(): string {
    return UserApiEvents::INIT_CANCEL_ACCOUNT;
  }

}
