# User API

## Contents of this file

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Introduction

The purpose of this module is to provide a REST API to work with user
accounts.

Integrates with the `verification` module to make sure that requests 
triggering a change have been verified by the user.

### Functionality

Provides REST resource plugins for the following functionality:

|Resource|Path|Description|
|-|-|-|
|`AdvancedUserRegistrationResource`|`/user-api/register`|Provides a resource to handle user account creation. This is a much improved version of the drupal core UserRegistrationRessource.|
|`ResendRegisterEmailResource`|`/user-api/register/resend-email`|Resends the registration email to the user.|
|`InitCancelAccountResource`|`/user-api/cancel-account/init`|Initializes account cancellation. Sends the confirmation email to cancel the account.|
|`CancelAccountResource`|`/user-api/cancel-account`|If properly verified, actually cancels the user account.|
|`InitSetPasswordResource`|`/user-api/set-password/init`|Initializes password reset. Sends the confirmation email to set a password.|
|`SetPasswordResource`|`/user-api/set-password`|If properly verified, actually sets the user's password.|

The `user_api_passwordless` module provides the following resources:

|Resource|Path|Description|
|-|-|-|
|`InitUnsetPasswordResource`|`/user-api/unset-password/init`|Initializes password reset. Sends the confirmation email to unset the password.|
|`UnsetPasswordResource`|`/user-api/unset-password`|If properly verified, actually unsets the user's password.|


The `user_api_email_confirm` module provides the following resources:

|Resource|Path|Description|
|-|-|-|
|`InitSetEmailResource`|`/user-api/set-email/init`|Initializes email change. Sends a confirmation email to the new email address. Optionally sends a notification email to the existing mail.|
|`SetEmailResource`|`/user-api/set-email`|If properly verified, actually changes the user's email address.|

## Requirements

The core module requires Drupal 10.3 or 11 and the following contrib modules:

- [Verification API](https://www.drupal.org/project/verification)
- [Simple OAuth](https://www.drupal.org/project/simple_oauth)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

E-Mails for the two submodules can be configured in the account settings if
enabled.

Otherwise each REST resource plugin must be configured manually, or via the
[Rest UI](https://www.drupal.org/project/restui) module.

## Maintainers

Current maintainers:

- Christoph Niedermoser ([@nimoatwoodway](https://www.drupal.org/u/nimoatwoodway))
- Christian Foidl ([@chfoidl](https://www.drupal.org/u/chfoidl))
